
CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Usage
 * Limitations
 * Contact

INTRODUCTION
------------

Form Clerk is a semi-automated way to input data into a Drupal form.

Form Clerk is intended to be used by non-programmers. It does not require
any PHP programming or database knowledge.

Form Clerk can be used to input data on a remote Drupal site. In other words,
you can install Form Clerk on one Drupal site and use it to input data on
other Drupal sites, if you are an authorized user on those sites.



REQUIREMENTS
------------

Your PHP environment must have 'Client URL Library', known as cURL, enabled.
Your PHP environment must have 'Document Object Model', known as DOM, enabled.

No Drupal modules are required.

INSTALLATION
------------

Install as usual, see http://drupal.org/node/70151 for further information.


CONFIGURATION
-------------

1. Configure user permissions in Administer >> User management >> Permissions >>
   Form_clerk module:

   - use form_clerk

     Users in roles with the "use form_clerk" permission can use
     Form Clerk. Note: Usually, using Form Clerk involves logging in to other
     sites with a separate userid and password. The permissions of the userid
     on the remote system determine what forms can be filled out. Those
     permissions are controlled by the administrator of the remote system.

Recommended practices:
 Create a special Form Clerk user on the target system. This makes it easier to
 track changes.


USAGE
-----

Recommended practices:
 Create a special Form Clerk user on the target system.

Create one or more input files. Visit http://drupalformclerk.com for examples.
Navigate to admin/settings/form_clerk.
Enter the input files in the 'Input Files' field.
Enter the password for the remote system in the password field.


SECURITY
--------
Form Clerk uses Drupal forms indirectly through the cURL interface in a way that
is very similar to directly entering the data in the form by yourseslf. As such,
it does not usually present any special security problems.

One exception to this is if you create Drupal users with Form Clerk. In this case,
the user passwords that you assign are present in the Form Clerk input file in plain text.
Therefore, these files should be carfully secured.



LIMITATIONS
-----------

Some form pages may be presented in a way that will not work with cURL.
For example, if the page uses AJAX to add a form to the original HTML,
cURL will not be able to read the form. In cases where this happens, you
may be able to access the form with another URL that will work with cURL.

For instance, an administrator can add new users at:
http://example.com/node#overlay=admin/people/create
However, cURL cannot read the form on the page, even though you can.
But if you access the same form at
http://example.com/admin/people/create
cURL can read the form.


CONTACT
-------

Current maintainers:
  Joe Casey (Joe Casey) - http://drupal.org/user/823758
