<?php

/**
 * @file
 * form_clerk UI functions.
 *
 */

function form_clerk_ui_build_form_field(&$form, &$form_state, $field='') {

  switch ($field) {
    // //////////////////////////////////////////////////////////////////////////////////
    case 'result_fieldset':
      $form['result'] = array(
        '#type' => 'fieldset',
        '#title' => t('Results'),
        '#tree' => TRUE,
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
      );
      break;

    // //////////////////////////////////////////////////////////////////////////////////
    case 'form_list':
      // Display the form info.
      $field_order = array('tag', 'type', 'label', 'name', 'value');
      $highlighted = array(1 => 'name', 2 => 'value');
      $markup = '<h3>URL: ' . $form_state['storage']['url'] . '</h3>';
      $form_list = $form_state['storage']['form_list'];
      if (count($form_list) == 0) {
        return 'No forms were found at that URL.';
      }
      for ($f = 0; $f < count($form_list); $f++) {
        $this_form = $form_list[$f];
        $table = array();
        $table['caption'] = 'Form ID: ' . $this_form['id'];
        $table['attributes'] = array('class' => 'form_clerk_table');
        $table['sticky'] = FALSE;
        $table['header'] = array();
        $table['rows'] = array();
        foreach ($field_order as $key => $field) {
            if (array_search($field, $highlighted)) {
              $table['header'][] = array('data' => $field,
                                         'class' => 'form_clerk_highlighted',
                                          );
            }
            else {
              $table['header'][] = $field;
            }
        }
        $elements = $this_form['elements'];
        foreach ($elements as $element) {
          $this_row = array();
          $this_row['data'] = array();
          $this_row['no_striping'] = TRUE;
          if ($element['type'] == 'hidden') {
            $this_row['class'] = array('form_clerk_hidden');
          }
          foreach ($field_order as $field) {
            if (array_search($field, $highlighted)) {
              $this_row['data'][] = array('data' => $element[$field],
                                          'class' => 'form_clerk_highlighted',
                                          );
            }
            else {
              $this_row['data'][] = $element[$field];
            }
          }
          $table['rows'][] = $this_row;
        }
        $markup .= theme('table', $table);
      }
      drupal_add_css(FORM_CLERK_PATH . '/form_clerk.css');
      $form['result']['form_list'] = array(
        '#type' => 'item',
        '#markup' => $markup,
      );
      break;

    // //////////////////////////////////////////////////////////////////////////////////
    // //////////////////////////////////////////////////////////////////////////////////
    // //////////////////////////////////////////////////////////////////////////////////
    case 'info_fieldset':
      $form['info'] = array(
        '#type' => 'fieldset',
        '#title' => t('Get Form Information'),
        '#tree' => TRUE,
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      );
      break;

    // //////////////////////////////////////////////////////////////////////////////////
    case 'info_login_fieldset':
      $form['info']['login'] = array(
        '#type' => 'fieldset',
        '#title' => t('Login Information'),
      );
      break;

    // //////////////////////////////////////////////////////////////////////////////////
    case 'info_form_url':
      $form['info']['form_url'] = array(
        '#type'     => 'textfield',
        '#size'     => 80,
        '#title'    => 'Form URL',
        '#default'    => isset($form_state['storage']['url']) ? $form_state['storage']['url'] : '',
//        '#required' => TRUE,
      );
      break;

    // //////////////////////////////////////////////////////////////////////////////////
    case 'info_login_needed':
      $form['info']['login']['login_needed'] = array(
        '#type'          => 'checkbox',
        '#title'         => t('Login Required?'),
        '#default_value' => 1,
      );
      break;

    // //////////////////////////////////////////////////////////////////////////////////
    case 'info_login_url':
      $form['info']['login']['login_url'] = array(
        '#type'     => 'textfield',
        '#size'     => 80,
        '#title'    => 'Login URL',
      );
      break;

    // //////////////////////////////////////////////////////////////////////////////////
    case 'info_password':
      $form['info']['login']['password'] = array(
        '#type'  => 'password',
        '#size'  => 20,
        '#title' => 'Password',
//        '#default'    => isset($form_state['storage']['password']) ? $form_state['storage']['password'] : '',
//        '#required' => TRUE,
      );
      break;

    // //////////////////////////////////////////////////////////////////////////////////
    case 'info_submit':
      $form['info']['submit-info'] = array(
        '#type'  => 'submit',
        '#value' => 'Get Form Info',
      );
      break;

    // //////////////////////////////////////////////////////////////////////////////////
    case 'info_user':
      $form['info']['login']['user'] = array(
        '#type'  => 'textfield',
        '#size'  => 20,
        '#title' => 'User name',
      );
      break;

    // //////////////////////////////////////////////////////////////////////////////////
    // //////////////////////////////////////////////////////////////////////////////////
    // //////////////////////////////////////////////////////////////////////////////////
    case 'input_fieldset':
      $form['input'] = array(
        '#type' => 'fieldset',
        '#title' => t('Input Data'),
        '#tree' => TRUE,
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
      );
      break;

    // //////////////////////////////////////////////////////////////////////////////////
    case 'input_file':
      $form['input']['input_file'] = array(
        '#type'  => 'textfield',
        '#title' => 'Form data input file',
      );
      break;

    // //////////////////////////////////////////////////////////////////////////////////
    case 'input_password':
      $form['input']['password'] = array(
        '#type'  => 'password',
        '#size'  => 20,
        '#title' => 'Password',
//        '#default'    => isset($form_state['storage']['password']) ? $form_state['storage']['password'] : '',
//        '#required' => TRUE,
      );
      break;

    // //////////////////////////////////////////////////////////////////////////////////
    case 'input_submit':
      $form['input']['submit-input'] = array(
        '#type'  => 'submit',
        '#value' => 'Input Form Data',
      );
      break;

    // //////////////////////////////////////////////////////////////////////////////////
    case 'report':
      // Write brief report.

      $markup = '<pre>' . $form_state['storage']['report'] . '</pre>';
      $form['result']['form_list'] = array(
        '#type' => 'item',
        '#markup' => $markup,
      );
      break;

    // //////////////////////////////////////////////////////////////////////////////////
    default:
      // We shouldn't get here. If we do, write some debugging messages.
      dpm($field, 'build_form_field: unknown $field');
      dpm($form_state, 'build_form_field: $form_state');
  }
}
