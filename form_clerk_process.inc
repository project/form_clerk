<?php

/**
 * @file
 * form_clerk utility functions.
 *
 */

/**
 * Main form_clerk configuration form builder.
 *
 * @param $form_state
 *  The multiform state argument.
 */
function form_clerk_process_input(&$form_state) {
//dpm($form_state, 'form_clerk_process_input $form_state');

  // The user can provide multiple input files.
  if (!isset($form_state['storage']['input']['input_file'])) {
      drupal_set_message(t('You must provide one or more input files.'), 'error');
      return;
    }
  $input_files = explode(',', $form_state['storage']['input']['input_file']);

  if (count($input_files) == 0) {
    drupal_set_message(t('You must provide one or more input files'), 'error');
    return;
  }

  foreach ($input_files as $input_file) {

    $input_file = trim($input_file);
    if ($input_file == '') {
      continue;
    }
    $form_state['storage']['report'] .= "Input: $input_file\n";
    if (! file_exists($input_file)) {
      drupal_set_message(t('File not found: @input_file',
                           array('@input_file' => check_plain($input_file))), 'error');
      return;
    }
    $input = fopen($input_file, "rb");
    if (! $input) {
      drupal_set_message(t('Unable to open file: @input_file',
                           array('!input_file' => check_plain($input_file))), 'error');
      return;
    }

    $line_number = 0;
    // Process lines one at a time.
    while (($line = fgets($input)) !== FALSE) {
      $line_number++;
      // Skip blank lines.
      if (trim($line) == '') {
        continue;
      }
      // Skip comments.
      if (substr($line, 0, 1) == $form_state['storage']['comment_flag']) {
        continue;
      }
      // Handle 'info_only' commands.
      elseif (preg_match('/^FC_info_only/', $line)) {
        $form_state['storage']['info_only'] = 'info_only';
        form_clerk_process_data('', $line_number, $input_file, $form_state);
      }
      // Handle configuration commands.
      elseif (substr($line, 0, 3) == 'FC_') {
        form_clerk_process_config($line, $line_number, $input_file, $form_state);
      }
      else {
        // Must be form input, process it.
        form_clerk_process_data($line, $line_number, $input_file, $form_state);
      }
      // Stop processing if an error has been reported.
      if (count(drupal_get_messages('error', FALSE)) > 0) {
        break 2;
      }
    }
    fclose($input);
  }
  return;
}


/**
 * form_clerk configuration helper.
 *
 * @param $form_state
 *  The multiform state argument.
 */
function form_clerk_process_data($line, $line_number, $input_file, &$form_state) {
  if (!form_clerk_process_ready($line, $line_number, $input_file, $form_state)) {
    return;
  }
//dpm("$line, $line_number, $input_file",  'form_clerk_process_data' );

  // Break the line into individual values.
  $delimiter = $form_state['storage']['delimiter'];
  $values = explode($delimiter, $line);
  $user_fields = array();
  $names = $form_state['storage']['names'];
  for ($i = 0; $i < count($values); $i++) {
    $name  = trim($names[$i]);
    $value = trim($values[$i]);
    if ($form_state['storage']['url_decode']) {
      $value = urldecode($value);
    }

    // Use the password given at startup, so passwords aren't stored in input files..
    if (($value == $form_state['storage']['password_indicator']) && ($name == 'pass')) {
      $value = $form_state['storage']['input']['password'];
    }

    $user_fields[$name] = $value;
  }
  $form_state['storage']['user_fields'] = $user_fields;

  // Input the form data.
  $form_state['storage']['input_file'] = $input_file;
  $form_state['storage']['line_number'] = $line_number;
  form_clerk_curl_do_form($form_state);

  return;
}

/**
 * form_clerk configuration helper.
 *
 * @param $form_state
 *  The multiform state argument.
 */
function form_clerk_process_config($line, $line_number, $input_file, &$form_state) {
  // Handle configuration commands.
  $delimiter = $form_state['storage']['delimiter'];
  if (preg_match('/^FC_names/', $line)) {
    // Might look like: FC_names = name, mail, pass[pass1], pass[pass2], status
    $line = substr($line, strpos($line, '=')+1);
    // Break the line into individual form field names.
    $names = explode($delimiter, $line);
    $form_state['storage']['names'] = $names;
  }
  else {
    preg_match('/FC_(\w*)\s*=(.*)\s*/', $line, $matches);
    $key = $matches[1];
    $value = trim($matches[2]);
    // Convert string pseudo-booleans to booleans.
    if ($value == 'TRUE') {
      $value = TRUE;
    }
    elseif ($value == 'FALSE') {
      $value = FALSE;
    }

    // Assume that all valid keys have previously defined defaults.
    if (!isset($form_state['storage'][$key])) {
      drupal_set_message(t('Unknown FC_ key "@key" in line #@line in file: @file',
        array(
              '@key'  => check_plain($key),
              '@line' => check_plain($line_number),
              '@file' => check_plain($input_file),
              )
        ),
        'warning');
    }

    $form_state['storage'][$key] = $value;

    // Handle 'info_only' commands.
//dpm($matches[1], 'matches 1');
    if (($matches[1] == 'info_only') && $form_state['storage']['info_only']) {
      form_clerk_process_data('', $line_number, $input_file, $form_state);
    }
  }
  return;
}

/**
 * form_clerk form info driver.
 *
 * @param $form_state
 *  The multiform state argument.
 */
function form_clerk_process_form_info(&$form_state) {
//dpm($form_state,  'form_clerk_process_form_info' );
  // Act as though we had an input file that looked like this:
  //  FC_url=http://example.com/login
  //  FC_form_id=user-login-form
  //  FC_names = name, pass
  //  form_clerk_admin, my_password
  //  FC_url=http://example.com/page-with-form
  //  FC_form_id=*
  //  FC_info_only=TRUE

  if (isset($form_state['storage']['info']['login_needed'])) {
    $line = 'FC_url=' . $form_state['storage']['info']['login_url'];
    form_clerk_process_config($line, 0, 'UI', $form_state);

    $line = 'FC_form_id=user-login-form';
    form_clerk_process_config($line, 0, 'UI', $form_state);

    $line = $form_state['storage']['info']['user'] . ',' . $form_state['storage']['info']['password'];
    form_clerk_process_data($line, 0, 'UI', $form_state);
  }

  $line = 'FC_names = name, pass';
  form_clerk_process_config($line, 0, 'UI', $form_state);

  $line = 'FC_url=' . $form_state['storage']['info']['form_url'];
  form_clerk_process_config($line, 0, 'UI', $form_state);

  $line = 'FC_form_id=*';
  form_clerk_process_config($line, 0, 'UI', $form_state);

  $line = 'FC_info_only=TRUE';
  form_clerk_process_config($line, 0, 'UI', $form_state);

  return;
}


function form_clerk_process_ready($line, $line_number, $input_file, &$form_state) {
  // We need to have already seen FC_fields to proceed.
  if (!isset($form_state['storage']['names'])) {
    drupal_set_message(t('FC_names must come before form values: Line #@line in file: @file',
      array('@line' => check_plain($line_number), '@file' => check_plain($input_file))), 'error');
    return FALSE;
  }
  // We need to have a form_id to proceed.
  if (!isset($form_state['storage']['form_id'])) {
    drupal_set_message(t('FC_form_id must come before form values: Line #@line in file: @file',
      array('@line' => check_plain($line_number), '@file' => check_plain($input_file))), 'error');
    return FALSE;
  }
  return TRUE;
}
